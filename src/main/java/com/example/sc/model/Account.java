package com.example.sc.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Account {
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="account_seq")
    @SequenceGenerator(
    	    name="account_seq",
    	    sequenceName="account_sequence",
    	    allocationSize=1,
    	    initialValue=20
    )
    private Long id;

    private String accountNumber;
    
    @ManyToOne(cascade = { CascadeType.ALL})
    private User owner;
    
    private BigDecimal balance;
    
    public String getAccountNumber() {
		return accountNumber;
	}
    
    public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
    
    public User getOwner() {
		return owner;
	}
    
    public void setOwner(User owner) {
		this.owner = owner;
	}
    
    public BigDecimal getBalance() {
		return balance;
	}
    
    public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
    
    @Override
    public String toString() {
        return "CreditCard{" +
                "id=" + id +
                ", accountNumber='" + accountNumber + '\'' +
                '}';
    }
    
    public static Account createAccount(String accountNumber) {
    	Account account = new Account();
    	account.setAccountNumber(accountNumber);
    	account.setBalance(BigDecimal.ZERO);
        return account;
    }
}
