package com.example.sc.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Transaction {
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="transaction_seq")
    @SequenceGenerator(
    	    name="transaction_seq",
    	    sequenceName="transaction_sequence",
    	    allocationSize=1,
    	    initialValue=20
    )
    private Long id;
	
	private BigDecimal amount;

	@ManyToOne(optional = true, cascade = { CascadeType.ALL})
	private Account depositAccount;

	@ManyToOne(optional = true, cascade = { CascadeType.ALL})
	private Account withdrawAccount;
    
    private BigDecimal currentDepositBalance;

    private BigDecimal currentWithdrawBalance;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @PrePersist
    protected void onCreate() {
    	created = new Date();
    }
    
	public BigDecimal getAmount() {
		return amount;
	}
	
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public Account getDepositAccount() {
		return depositAccount;
	}
	
	public void setDepositAccount(Account depositAccount) {
		this.depositAccount = depositAccount;
	}
	
	public Account getWithdrawAccount() {
		return withdrawAccount;
	}
	
	public void setWithdrawAccount(Account withdrawAccount) {
		this.withdrawAccount = withdrawAccount;
	}
	
	public BigDecimal getCurrentDepositBalance() {
		return currentDepositBalance;
	}
	
	public void setCurrentDepositBalance(BigDecimal currentDepositBalance) {
		this.currentDepositBalance = currentDepositBalance;
	}
	
	public BigDecimal getCurrentWithdrawBalance() {
		return currentWithdrawBalance;
	}
	
	public void setCurrentWithdrawBalance(BigDecimal currentWithdrawBalance) {
		this.currentWithdrawBalance = currentWithdrawBalance;
	}
	
	public Date getCreated() {
		return created;
	}
	
	public void setCreated(Date created) {
		this.created = created;
	}
	
	// TODO create a toString method which prints the deposit/withdraw account data based on the account number or user name
	@Override
    public String toString() {
        return "Transaction: {" +
                " amount='" + amount + '\'' +
                ", created='" + created + '\'' +
                ", depositAccountNumber='" + depositAccount.getAccountNumber() + '\'' +
                ", currentDepositBalance='" + currentDepositBalance + '\'' +
                ", withdrawAccountNumber='" + withdrawAccount.getAccountNumber() + '\'' +
                ", currentWithdrawBalance='" + currentWithdrawBalance + '\'' +
                " }";
    }
	
	public static Transaction createTransfer(BigDecimal amount, Account depositAccount, Account withdrawAccount) {
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setDepositAccount(depositAccount);
		transaction.setWithdrawAccount(withdrawAccount);
		// TODO check needed, if one of the balances goes under zero
		if(depositAccount != null) {
			transaction.setCurrentDepositBalance(depositAccount.getBalance().add(amount));
		}
		if(withdrawAccount != null) {
			transaction.setCurrentWithdrawBalance(withdrawAccount.getBalance().subtract(amount));
		}
        return transaction;
    }
	
	public static Transaction createDeposit(BigDecimal amount, Account depositAccount) {
		return createTransfer(amount, depositAccount, null);
	}
	
	public static Transaction createWithdraw(BigDecimal amount, Account withdrawAccount) {
		return createTransfer(amount, null, withdrawAccount);
	}
}
