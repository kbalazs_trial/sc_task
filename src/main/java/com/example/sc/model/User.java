package com.example.sc.model;

import static javax.persistence.FetchType.EAGER;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_seq")
    @SequenceGenerator(
    	    name="user_seq",
    	    sequenceName="user_sequence",
    	    allocationSize=1,
    	    initialValue=20
    )
    private Long id;

    private String name;
    
    @OneToMany(cascade = { CascadeType.ALL}, fetch = EAGER, mappedBy = "owner", orphanRemoval=true)
    private Set<Account> accounts = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Account> creditCards) {
        this.accounts = creditCards;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", accounts=" + accounts +
                '}';
    }

    public static User createUser(String name, Collection<Account> accounts) {
        User user = new User();
        user.setName(name);

        for (Account account : accounts) {
        	account.setOwner(user);
            user.getAccounts().add(account);
        }

        return user;
    }
}