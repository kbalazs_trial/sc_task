package com.example.sc;

import java.math.BigDecimal;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.sc.model.Transaction;
import com.example.sc.repositories.TransactionRepository;
import com.example.sc.repositories.UserRepository;

@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}

	@Bean
	public CommandLineRunner demo(UserRepository userRepository, TransactionRepository transactionRepository) {
		return (args) -> {
			// save a couple of customers
			userRepository.createWithAccounts("First User", "123", "456");
			userRepository.createWithAccounts("Second User", "789");
			
			transactionRepository.createDeposit(new BigDecimal(1000l), "123");
			transactionRepository.createWithdraw(new BigDecimal(200l), "123");
			
			transactionRepository.createTransfer(new BigDecimal(500l), "456", "123");
			transactionRepository.createTransfer(new BigDecimal(300l), "789", "123");
			
			Collection<Transaction> firstUserTransactions = transactionRepository.findByUserName("First User");
			log.info("find the transactions of the first user:");
			log.info("-------------------------------");
			for (Transaction transaction : firstUserTransactions) {
				log.info(transaction.toString());
			}
			log.info("");

			Collection<Transaction> secondUserTransactions = transactionRepository.findByUserName("Second User");
			log.info("find the transactions of the first user:");
			log.info("-------------------------------");
			for (Transaction transaction : secondUserTransactions) {
				log.info(transaction.toString());
			}
			log.info("");
			
		};
	}

}