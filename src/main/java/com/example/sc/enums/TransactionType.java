package com.example.sc.enums;

public enum TransactionType {

	DEPOSIT,
	WITHDRAW,
	ALL
}
