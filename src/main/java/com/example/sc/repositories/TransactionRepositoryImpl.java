package com.example.sc.repositories;


import java.math.BigDecimal;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.sc.filters.TransactionFilter;
import com.example.sc.model.Account;
import com.example.sc.model.Transaction;

@Transactional
public class TransactionRepositoryImpl implements TransactionRepositoryCustom {
	@PersistenceContext
    private EntityManager entityManager;

	@Autowired
	private AccountRepository accountRepository;
	
    @Override
    public Transaction createTransfer(BigDecimal amount, String depositAccountNumber, String withdrawAccountNumber) {
    	Account depositAccount = getAccount(depositAccountNumber);
    	Account withdrawAccount = getAccount(withdrawAccountNumber);
        Transaction transaction = Transaction.createTransfer(amount, depositAccount, withdrawAccount);
        entityManager.persist(transaction);
        depositAccount.setBalance(transaction.getCurrentDepositBalance());
        withdrawAccount.setBalance(transaction.getCurrentWithdrawBalance());
        entityManager.merge(depositAccount);
        entityManager.merge(withdrawAccount);
        return transaction;
    }


    @Override
    public Transaction createDeposit(BigDecimal amount, String depositAccountNumber){
    	Account depositAccount = getAccount(depositAccountNumber);;
    	Transaction transaction = Transaction.createDeposit(amount, depositAccount);
        entityManager.persist(transaction);
        depositAccount.setBalance(transaction.getCurrentDepositBalance());
        entityManager.merge(depositAccount);
        return transaction;
    }


    @Override
    public Transaction createWithdraw(BigDecimal amount, String withdrawAccountNumber){
    	Account withdrawAccount = getAccount(withdrawAccountNumber);
        Transaction transaction = Transaction.createWithdraw(amount, withdrawAccount);
        entityManager.persist(transaction);
        withdrawAccount.setBalance(transaction.getCurrentWithdrawBalance());
        entityManager.merge(withdrawAccount);
        return transaction;
    }
    
    @Override
    public Collection<Transaction> filter(TransactionFilter filter) {
    	// TODO
    	return null;
    	
    }
    
    private Account getAccount(String accountNumber) {
    	Collection<Account> accounts = accountRepository.findByAccountNumber(accountNumber);
    	if(!accounts.isEmpty()) {
    		if(accounts.size() == 1) {
    			return accounts.iterator().next();
    		} else {
    			throw new IllegalStateException("Two accounts has the same accountNumber: " + accountNumber);
    		}	
    	}
    	throw new IllegalArgumentException("The account is not found with the following accountNumber: " + accountNumber);
    }
    
    public void setAccountRepository(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

}
