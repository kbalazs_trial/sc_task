package com.example.sc.repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.sc.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long>, TransactionRepositoryCustom {
    
    // this should be solved with criteria API
/*    Collection<Transaction> findByDepositAccountOwnerName(String name);

    Collection<Transaction> findByWithdrawAccountOwnerName(String name);

    Collection<Transaction> findByCreatedAfter(Date after);

    Collection<Transaction> findByCreatedBefore(Date before);
*/

    @Query("select t from Transaction t where (t.depositAccount is not null AND t.depositAccount.owner.name = ?1) OR (t.withdrawAccount is not null AND t.withdrawAccount.owner.name = ?1)")
    Collection<Transaction> findByUserName(String name);

}
