package com.example.sc.repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.sc.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Collection<Account> findByAccountNumber(String accountNumber);

}
