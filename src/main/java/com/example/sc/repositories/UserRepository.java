package com.example.sc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.sc.model.User;

public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom {

}
