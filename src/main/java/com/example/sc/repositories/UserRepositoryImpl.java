package com.example.sc.repositories;

import static com.example.sc.model.Account.createAccount;
import static com.example.sc.model.User.createUser;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.example.sc.model.Account;
import com.example.sc.model.User;

@Transactional
public class UserRepositoryImpl implements UserRepositoryCustom {
	@PersistenceContext
    private EntityManager entityManager;

    @Override
    public User createWithAccounts(String name, String... accountNumber) {
    	Collection<Account> accounts = Arrays.asList(accountNumber).stream().map(a -> { return createAccount(a); }).collect(Collectors.toSet());
        User user = createUser(name, accounts);
        entityManager.persist(user);
        return user;
    }
}
