package com.example.sc.repositories;

import java.math.BigDecimal;
import java.util.Collection;

import com.example.sc.filters.TransactionFilter;
import com.example.sc.model.Transaction;

public interface TransactionRepositoryCustom {

	Transaction createTransfer(BigDecimal amount, String depositAccountNumber, String withdrawAccountNumber);

	Transaction createDeposit(BigDecimal amount, String depositAccountNumber);

	Transaction createWithdraw(BigDecimal amount, String withdrawAccountNumber);
	
	Collection<Transaction> filter(TransactionFilter filter);
}
