package com.example.sc.repositories;

import com.example.sc.model.User;

public interface UserRepositoryCustom {

	User createWithAccounts(String name, String... accountNumber);

}
