package com.example.sc.filters;

import java.util.Date;

import com.example.sc.enums.TransactionType;

/**
 * Filter for the criteria query.
 * @author kbalazs
 *
 */
public class TransactionFilter {

	private Date dateAfter;

	private Date dateBefore;

	private String userName;
	
	private TransactionType transactionType = TransactionType.ALL;
	
	public class TransactionFilterBuilder {
		TransactionFilter filter = new TransactionFilter();
		
		public TransactionFilterBuilder(String userName) {
			filter.userName = userName;
		}
		
		public TransactionFilterBuilder dateAfter(Date dateAfter) {
			filter.dateAfter = dateAfter;
			checkDates();
			return this;
		}
		
		public TransactionFilterBuilder dateBefore(Date dateBefore) {
			filter.dateBefore = dateBefore;
			checkDates();
			return this;
		}
		
		public TransactionFilterBuilder onlyDeposits() {
			filter.transactionType = TransactionType.DEPOSIT;
			return this;
		}
		
		public TransactionFilterBuilder onlyWithdraws() {
			filter.transactionType = TransactionType.WITHDRAW;
			return this;
		}
		
		private void checkDates() {
			if(filter.dateAfter != null && filter.dateBefore != null && 
					filter.dateBefore.after(filter.dateAfter)) {
				throw new IllegalArgumentException("The dateBefore can not be after the dateAfter!");
			}
		}
		
		public TransactionFilter build() {
			return filter;
		}
	}
	
	public String getUserName() {
		return userName;
	}
	
	public TransactionType getTransactionType() {
		return transactionType;
	}
	
	public Date getDateAfter() {
		return dateAfter;
	}
	
	public Date getDateBefore() {
		return dateBefore;
	}
}
